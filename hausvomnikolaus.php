<?php

/* @var $this SiteController */
?>
<h5>Das Haus vom Nikolaus</h5>

<?php

$punkte = array(10, 20, 21, 30, 31);
$successCountFull = 0;
foreach ($punkte as $startpunkt){
    $successCount = 0;
    toNext(null, $startpunkt, $punkte, array(), $successCount);
    $successCountFull += $successCount;
    $successCount = 0;
}
echo "Es gibt $successCountFull die Erfolgreich das Haus vom Nikolaus fertig zeichnen";

function toNext($prev, $punkt, $avail, $path, &$successCount) {
    if (isset($prev))
        array_push($path, (int) ($prev . $punkt));
    $posibleNext = array($punkt - 10, $punkt - 11, $punkt - 9, $punkt - 1, $punkt + 1, $punkt + 9, $punkt + 10, $punkt + 11);
    $next = array();
    for ($i = 0; $i < count($posibleNext); $i++) {
        if (array_search($posibleNext[$i], $avail) !== false && isNewPath($punkt, $posibleNext[$i], $path)) {
            array_push($next, $posibleNext[$i]);
        }
    }
    if (count($next) === 0 && count($path) === 8) {
        echo "<span><span style='color:green'>Erfolg: </span>";
        print_r($path);
        echo "</span><br>";
        $successCount++;
        return;
    } else if (count($next) === 0) {
        return;
    } else {
        foreach ($next as $n) {
            toNext($punkt, $n, $avail, $path,$successCount);
        }
    }
}

function isNewPath($start, $end, $path) {
    return (array_search($start . $end, $path) === false && array_search($end . $start, $path) === false);
}
?>